# mandel

Simple Mandelbrot & Julia set viewer written in C++ using SDL, 
originally based on [this video](https://youtu.be/fWP29aVVycA) tutorial by Top Shelf Technology. 

## Keybinds

| Key | Action |
|-----|--------|
| Left click   | Change value of z or c depending on Mandelbrot or Julia context |
| Arrow keys   | Pan      |
| +            | Zoom in  |
| -            | Zoom out |
| j | Switch between Mandelbrot mode and Julia mode |
| r | Reset the value of z/c to (0,0) |
| c | Recenter and reset zoom |

## Building

You need the libraries SDL2 and SDL\_ttf installed. There is a Makefile included for compilation on Linux. 
I have not figured out how to compile for Windows but it should be possible thanks to the interoperability 
of SDL. OSX should also be possible, but I don't have any Apple hardware to test on.

### For Linux

Clone the repository using `git clone https://gitlab.com/Fjalar/mandel.git`, then change directory into 
the project folder using `cd mandel`, build the binary file using `make`, then finally run the program 
with the command `./mandel`.

## TODO

- [x] Panning
- [x] Display constant used
- [ ] Commandline arguments for resolution, iteration count, etc.
- [ ] Proper program structure
- [ ] Figure out how to build for Windows
- [ ] Performance improvement using some sort of cache on isInSet function calls?
- [ ] ???

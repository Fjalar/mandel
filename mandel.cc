#include <SDL2/SDL.h>
#include <numeric>
#include <complex>
#include <cmath>
#include <iostream>
#include <SDL2/SDL_ttf.h>
#include <string>
#include "SetHandler.h"
#include "Viewer.h"

const int xRes = 1200;
const int yRes = 1000;

double xBoundLeft = -3.0;
double xBoundRight = 2.0;

double yBoundTop = -2.0;
double yBoundBottom = 2.0;

const int maxIters = 128;
const double maxBound = 4;

bool julia = false;

std::complex<double> zStart(0,0);

SetHandler* sethandler = new SetHandler(maxIters, maxBound, zStart);

/*
void draw(SDL_Renderer* renderer) {
  for (int x = 0; x < xRes; x++) {
      for (int y = 0; y < yRes; y++) {

        double pointX = std::lerp(xBoundLeft, xBoundRight, static_cast<double>(x)/xRes);
        double pointY = std::lerp(yBoundTop, yBoundBottom, static_cast<double>(y)/yRes);
        double iters;

        if (julia) {
          iters = sethandler.isInJulia(std::complex<double>(pointX, pointY));
        }
        else {
          iters = sethandler.isInMandel(std::complex<double>(pointX, pointY));
        }

        if (iters == 0) {
          SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        }
        else {
          double r = std::fmod((static_cast<double>(iters)/maxIters * 255)*11, 255);
          double g = std::fmod((static_cast<double>(iters)/maxIters * 255)*14, 255);
          double b = std::fmod((static_cast<double>(iters)/maxIters * 255)*17, 255);

          SDL_SetRenderDrawColor(renderer, r, g, b, 255);
        }

        SDL_RenderDrawPoint(renderer, x, y);
      }
    }
    
    TTF_Font* font = TTF_OpenFont("font/cmunorm.ttf", 48);
    
    std::string displayString = "(" + std::to_string(zStart.real()) + ", " + std::to_string(zStart.imag()) + ")";

    SDL_Color col = SDL_Color(255,255,255,255);
    SDL_Surface* textSurface = TTF_RenderText_Solid(font, displayString.c_str(), col);
    SDL_Texture* textTexture = SDL_CreateTextureFromSurface(renderer, textSurface);
    SDL_Rect textRect = {30, 30, textSurface->w, textSurface->h};
    SDL_FreeSurface(textSurface);
    SDL_RenderCopy(renderer, textTexture, NULL, &textRect);
    SDL_RenderPresent(renderer);
    TTF_CloseFont(font);
    //std::cout << "New frame rendered\n";
}

void zoomIn() {
  double xRange = xBoundRight - xBoundLeft;
  double yRange = yBoundBottom - yBoundTop;

  xBoundLeft += xRange * 0.05;
  xBoundRight -= xRange * 0.05;
  yBoundTop += yRange * 0.05;
  yBoundBottom -= yRange * 0.05;
}

void zoomOut() {
  double xRange = xBoundRight - xBoundLeft;
  double yRange = yBoundBottom - yBoundTop;

  xBoundLeft -= xRange * 0.05;
  xBoundRight += xRange * 0.05;
  yBoundTop -= yRange * 0.05;
  yBoundBottom += yRange * 0.05;
}

void pan(int x, int y) {
  double xRange = xBoundRight - xBoundLeft;
  double yRange = yBoundBottom - yBoundTop;

  xBoundLeft += xRange * x * 0.05;
  xBoundRight += xRange * x * 0.05;
  yBoundTop += yRange * y * 0.05;
  yBoundBottom += yRange * y * 0.05;
}
*/

bool mouseHeld = false;
bool running = true;

void handleInput(SDL_Event event, Viewer& viewer, SetHandler& sethandler) { //, SDL_Renderer &renderer) {
  switch (event.type) {
    case SDL_QUIT:
      running = false;
      SDL_Quit();
      break;
    case SDL_MOUSEBUTTONDOWN:
      mouseHeld = true;
      break;
    case SDL_MOUSEBUTTONUP:
      mouseHeld = false;
      break;
    case SDL_KEYDOWN:
      bool shouldDraw = true;
      switch (event.key.keysym.sym) {
        case SDLK_LEFT:
          viewer.pan(-1, 0);
          break;
        case SDLK_RIGHT:
          viewer.pan(1, 0);
          break;
        case SDLK_UP:
          viewer.pan(0, -1);
          break;
        case SDLK_DOWN:
          viewer.pan(0, 1);
          break;
        case SDLK_PLUS:
          viewer.zoom_in();
          //std::cout << "Plus pressed" << "\n";
          break;
        case SDLK_MINUS:
          viewer.zoom_out();
          //std::cout << "Minus pressed" << "\n";
          break;
        case SDLK_ESCAPE:
          running = false;
          shouldDraw = false;
          SDL_Quit();
          break;
        case SDLK_c:
          viewer.reset();
          break;
        case SDLK_r:
          sethandler.setConstant(std::complex<double>(0,0));
          break;
        case SDLK_j:
          viewer.julia = !julia;
          //std::cout << "Julia is " << julia << "\n";
          break;
        default:
          shouldDraw = false;
      }
      if (shouldDraw) {
        viewer.draw();
      }
  }
}

int main() {

  SDL_SetHint(SDL_HINT_VIDEODRIVER, "wayland,x11");
  SDL_Init(SDL_INIT_EVERYTHING);
  TTF_Init();
  SDL_Window* window = nullptr;
  SDL_Renderer* renderer = nullptr;
  SDL_CreateWindowAndRenderer(xRes, yRes, 0, &window, &renderer);
  Viewer* viewer = new Viewer(*renderer, *sethandler);

  viewer->draw();

  int fps = 60;
  int desiredDelta = 1000/fps;

  while (running) {
    int startLoop = SDL_GetTicks();
    SDL_Event event;
    while(SDL_PollEvent(&event)) {
      handleInput(event, *viewer, *sethandler); // *renderer);
    }
    if (mouseHeld) {
      int mouseX;
      int mouseY;
      SDL_GetMouseState(&mouseX, &mouseY);
      double a = std::lerp(xBoundLeft, xBoundRight, static_cast<double>(mouseX)/xRes);
      double b = std::lerp(yBoundTop, yBoundBottom, static_cast<double>(mouseY)/yRes);
      sethandler->setConstant(std::complex<double>(a, b));
      viewer->draw();
      //std::cout << mouseX << " " << mouseY << "\n";
    }
    int delta = SDL_GetTicks() - startLoop;
    if (delta < desiredDelta) {
      SDL_Delay(desiredDelta - delta);
    }
  }
  return 0;
}

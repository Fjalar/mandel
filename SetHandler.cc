#include <complex>
#include "SetHandler.h"

SetHandler::SetHandler(int maxItersIn, int maxBoundIn, std::complex<double> aIn) {
  maxIters = maxItersIn;
  maxBound = maxBoundIn;
  a = aIn;
}

int SetHandler::isInMandel(std::complex<double> c) {
  std::complex<double> z = a;
  for (int i = 0; i < maxIters; ++i) {
    z = z*z + c;
    if (std::norm(z) > maxBound) {
      return i;
    }
  }
  return 0;
}

int SetHandler::isInJulia(std::complex<double> z) {
  std::complex<double> c = a;
  for (int i = 0; i < maxIters; ++i) {
    z = z*z + c;
    if (std::norm(z) > maxBound) {
      return i;
    }
  }
  return 0;
}

void SetHandler::setConstant(std::complex<double> aIn) {
  a = aIn;
}

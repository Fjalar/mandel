#ifndef SETHANDLER_H
#define SETHANDLER_H

#include <complex>

class SetHandler {
public:
  // Construct, with maxIters, maxBound and chosen constant for z in Mandelbrot
  // and c in Julia set calculation
  SetHandler(int maxItersIn, int maxBoundIn, std::complex<double> aIn);

  // Check if complex value is in the Mandelbrot set with starting z = a
  int isInMandel(std::complex<double> c);

  // Check if complex value is in the Julia set with c = a
  int isInJulia(std::complex<double> z);

  void setConstant(std::complex<double> aIn);
  
  std::complex<double> a;

private:
  int maxIters;
  int maxBound;
  
};

#endif

#ifndef VIEWER_H
#define VIEWER_H

#include <SDL2/SDL.h>
#include <complex>
#include <SDL2/SDL_ttf.h>
#include <string>
#include "SetHandler.h"

class Viewer {
private:
  int x_res;
  int y_res;
  double x_bound_left;
  double x_bound_right;
  double y_bound_top;
  double y_bound_bottom;
  SDL_Renderer* renderer;
  SetHandler* sethandler;
  TTF_Font* font;
public:

  bool julia;
  Viewer(SDL_Renderer& renderer_in, SetHandler& sethandler_in);
  void draw();
  void zoom_in();
  void zoom_out();
  void pan(int x, int y);
  void reset();
};
#endif

#include <SDL2/SDL.h>
#include <complex>
#include <SDL2/SDL_ttf.h>
#include <string>
#include "SetHandler.h"
#include "Viewer.h"
#include <cmath>

Viewer::Viewer(SDL_Renderer& renderer_in, SetHandler& sethandler_in) {
  renderer = &renderer_in;
  sethandler = &sethandler_in;

  x_res = 1200;
  y_res = 1000;
  
  x_bound_left = -3.0;
  x_bound_right = 2.0;

  y_bound_top = -2.0;
  y_bound_bottom = 2.0;

  julia = false;

  font = TTF_OpenFont("font/cmunorm.ttf", 48);
}

void Viewer::draw() {
  for (int x = 0; x < x_res; ++x) {
    for (int y = 0; y < y_res; ++y) {
      double point_x = std::lerp(x_bound_left, x_bound_right, static_cast<double>(x)/x_res);
      double point_y = std::lerp(y_bound_top, y_bound_bottom, static_cast<double>(y)/y_res);
      int iters;

      if (julia) {
        iters = sethandler->isInJulia(std::complex<double>(point_x, point_y));
      }
      else {
        iters = sethandler->isInMandel(std::complex<double>(point_x, point_y));
      }
      if (iters == 0) {
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
      }
      else {

        double r = std::fmod((static_cast<double>(iters)/128.0 * 255.0)*11.0, 255);
        double g = std::fmod((static_cast<double>(iters)/128 * 255.0)*14.0, 255);
        double b = std::fmod((static_cast<double>(iters)/128 * 255.0)*17.0, 255);

        SDL_SetRenderDrawColor(renderer, r, g, b, 255);
      }
      SDL_RenderDrawPoint(renderer, x, y);
    }
  }

  std::string display_string = "(" + std::to_string(sethandler->a.real()) + ", " + std::to_string(sethandler->a.imag()) + ")";

  SDL_Color col = SDL_Color(255, 255, 255, 255);
  SDL_Surface* text_surface = TTF_RenderText_Solid(font, display_string.c_str(), col);
  SDL_Texture* text_texture = SDL_CreateTextureFromSurface(renderer, text_surface);
  SDL_Rect text_rect = {30, 30, text_surface->w, text_surface->h};
  SDL_FreeSurface(text_surface);
  SDL_RenderCopy(renderer, text_texture, NULL, &text_rect);
  SDL_RenderPresent(renderer);
}


void Viewer::zoom_in() {
  double x_range = x_bound_right - x_bound_left;
  double y_range = y_bound_bottom - y_bound_top;

  x_bound_left += x_range * 0.05;
  x_bound_right -= x_range * 0.05;
  y_bound_top += y_range * 0.05;
  y_bound_bottom -= y_range * 0.05;
}


void Viewer::zoom_out() {
  double x_range = x_bound_right - x_bound_left;
  double y_range = y_bound_bottom - y_bound_top;

  x_bound_left -= x_range * 0.05;
  x_bound_right += x_range * 0.05;
  y_bound_top -= y_range * 0.05;
  y_bound_bottom += y_range * 0.05;

}
void Viewer::pan(int x, int y) {
  double x_range = x_bound_right - x_bound_left;
  double y_range = y_bound_bottom - y_bound_top;

  x_bound_left += x_range * x * 0.05;
  x_bound_right += x_range * x * 0.05;
  y_bound_top += y_range * y * 0.05;
  y_bound_bottom += y_range * y * 0.05;
}

void Viewer::reset() {
  x_bound_left = -3.0;
  x_bound_right = 2.0;

  y_bound_top = -2.0;
  y_bound_bottom = 2.0;
}
 
